/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.book_bank;

/**
 *
 * @author Maintory_
 */
public class book_bank {
    String name;
    double balance;
    
    book_bank(String name, double money){
        this.name = name;
        this.balance = money;
    }
    void deposit(double money){
        if(money <= 0){
            System.out.println("Money must more than = 0");
        }
        else{
        this.balance = this.balance + money;
        }
    }
    void withdraw(double money){
        if(money <= 0){
            System.out.println("Money must more than = 0");
            return;
        }
        else{
        if(this.balance >= money){
        this.balance = this.balance - money;
        }
        else{
            System.out.println(this.name + " Insufficient funds to withdraw !");
            return;
        }}
    }
    
}
